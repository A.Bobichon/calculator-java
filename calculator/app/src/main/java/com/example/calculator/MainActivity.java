package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.icu.lang.UCharacter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // this function split the value in the screen
    public String[] GetScreenVal (TextView screen){
        return screen.getText().toString().split(" ");
    };


    public void Reset(TextView screen){
        screen.setText("");
    };

    public String AffScreen(TextView screen, String val){
        String data = screen.getText().toString();
        data += val;
        return data;
    };

    // the calcul function
    public int calcul(int num1, int num2, String sign){

        int result = 0;

        if ( sign.equals("+")){ result = num1 + num2; }
        if ( sign.equals("-")){ result = num1 - num2; }
        if ( sign.equals("/")){ result = num1 / num2; }
        if ( sign.equals("X")){ result = num1 * num2; }
        if ( sign.equals("%")){ result = (num2*num1) / 100;  }

        return result;
    };

    public void UpdateScreen(View v, TextView screen){
        Button btn = (Button) findViewById(v.getId());
        String txt = btn.getText().toString();

        Boolean test;

        try {
            Integer.parseInt(txt);
            test = true;
        } catch(NumberFormatException e){
            test = false;

        }

        if (test) {

            String data = screen.getText().toString() + txt;
            screen.setText(data);

        } else {
            String data = screen.getText().toString() + " " + txt + " ";
            screen.setText(data);
        }
    };

    public void actionScreen(int id, final TextView screen){
        final Button btn = findViewById(id);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if( GetScreenVal(screen).length <= 3) {
                        UpdateScreen(view, screen);

                    }
                }
            });
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView screenVal = findViewById(R.id.screener);

        // Button reset
        final Button reset = findViewById(R.id.AC);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Reset(screenVal);
            }
        });

        //Btn number action
        actionScreen(R.id.zero, screenVal);
        actionScreen(R.id.un, screenVal);
        actionScreen(R.id.deux ,screenVal);
        actionScreen(R.id.trois, screenVal);
        actionScreen(R.id.quatre, screenVal);
        actionScreen(R.id.cinq, screenVal);
        actionScreen(R.id.six, screenVal);
        actionScreen(R.id.sept, screenVal);
        actionScreen(R.id.huit, screenVal);
        actionScreen(R.id.neuf, screenVal);

        //Btn add number/signe
        actionScreen(R.id.addition, screenVal);
        actionScreen(R.id.multiplication, screenVal);
        actionScreen(R.id.soustraction, screenVal);
        actionScreen(R.id.division, screenVal);
        actionScreen(R.id.modulo, screenVal);

        // Btn cal
        final Button resultGiven = findViewById(R.id.egal);
        resultGiven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               String[] myTab = GetScreenVal(screenVal);


              try {
                   if (myTab.length == 3) {
                        int num1 = Integer.parseInt(myTab[0]);
                        int num2 = Integer.parseInt(myTab[2]);
                        String sign = myTab[1];

                        int result = calcul(num1, num2, sign);
                        Reset(screenVal);
                        String data = screenVal.getText().toString() + result;

                        screenVal.setText(data);
                   } else {
                       Reset(screenVal);
                   }
               } catch(NumberFormatException e){
                   Reset(reset);
               }

            }
        });
    }
}